<?php

namespace app\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property string $username
 * @property string $email
 * @property string $text
 * @property int|null $completed
 * @property int $updated_by_user_id
 *
 * @property User $updatedByUser
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'email', 'text'], 'required'],
            [['text'], 'string'],
            [['completed', 'updated_by_user_id'], 'integer'],
            [['username'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 320],
            [['email'], 'email'],
            [['updated_by_user_id'], 'default', 'value' => null],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Имя пользователя',
            'email' => 'Email',
            'text' => 'Задача',
            'completed' => 'Статус',
            'updated_by_user_id' => 'Обновлено пользователем',
        ];
    }

    /**
     * Gets query for [[UpdatedByUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedByUser()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by_user_id']);
    }
}
