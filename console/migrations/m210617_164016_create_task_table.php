<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%task}}`.
 */
class m210617_164016_create_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%task}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string(255)->notNull(),
            'email' => $this->string(320)->notNull(),
            'text' => $this->text()->notNull(),
            'completed' => $this->boolean()->defaultValue(false),
            'updated_by_user_id' => $this->integer()
        ]);

        // creates index for column `country_id`
        $this->createIndex(
            '{{%idx-task-user_id}}',
            '{{%task}}',
            'updated_by_user_id'
        );

        $this->addForeignKey(
            '{{%fk-task-user_id}}',
            '{{%task}}',
            'updated_by_user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%country}}`
        $this->dropForeignKey(
            '{{%fk-task-user_id}}',
            '{{%task}}'
        );

        // drops index for column `country_id`
        $this->dropIndex(
            '{{%idx-task-user_id}}',
            '{{%task}}'
        );

        $this->dropTable('{{%task}}');
    }
}
